class sternInputForm extends HTMLElement {
  constructor() {
    super();

    this.label = this.getAttribute("label");
    this.separator = this.getAttribute("separator") === "true";
    this.validationStyle = "";

    var shadow = this.attachShadow({
      mode: "open"
    });
    var layout = this.getAttribute("layout")
      ? this.getAttribute("layout")
      : "table";
    var labelWidth = this.getAttribute("labelWidth");
    var controlWidth = this.getAttribute("controlWidth");
    var labelAlign = this.getAttribute("labelAlign");

    var validationType = this.getAttribute("validationType")
      ? this.getAttribute("validationType")
      : "text";

    switch (validationType) {
      case "color":
        validationStyle = "error-text";
        break;
      case "border":
        validationStyle = "error-border";
        break;
    }

    var template = this.getTemplate(
      layout,
      labelAlign,
      labelWidth,
      controlWidth
    );
    shadow.innerHTML = template;
    window.addEventListener('parent-attr', function(event) {
      this.formValidations = event.data.formValidations
      this.id = event.data.id
    })
  }

  getTemplate(layout, labelAlign, labelWidth, controlWidth,validationType) {
    var template = "";
    var labelWidthClass = "w-" + labelWidth;
    var controlWidthClass = "w-" + controlWidth;
    var labelAlignClass = "align-" + labelAlign;

    switch (layout) {
      case "node":
        template = template;
        break;
      case "block":
        var floatClass = this.getFloatElement(labelAlign);
        template = `
            <div class="form-wrapper-ux">
                <div class="form-container-ux">
                    <div class="form-row-ux d-block ${controlWidthClass} ${floatClass}">
                        <div class="form-control-ux" ng-show="label">
                            <div class="inner-cell ${labelAlignClass}">
                                <label for="{{id}}" class="form-lb">{{ label | translate }} block</label>
                            </div>
                        </div>
                        <div class="form-control-ux ${floatClass}">
                            <div class="inner-cell">
                                ${
                                  validationType === "text" &&
                                  (this.formValidations && this.formValidations.$submitted ||
                                    this.formValidations && this.formValidations[id].$touched) &&
                                  this.formValidations && this.formValidations[id].$invalid
                                    ? `<div>
                                    <span class="f-smaller">{{ 'invalid-field' | translate }}</span>
                                </div>`
                                    : ""
                                }
                            <div>
                        </div>
                    </div>
                </div>
            </div>
        `;
        break;
      case "compact":
        var floatClass = this.getFloatElement(this.labelAlign);
        template = `
            <div class="form-wrapper-ux">
                <div class="form-container-ux">
                    <div class="form-row-ux d-block ${controlWidthClass} ${floatClass}">
                        <div class="form-control-ux" ng-show="label">
                            <div class="inner-cell ${labelAlignClass}">
                                <label for="{{id}}" class="form-lb">{{ label | translate }} compact</label>
                            </div>
                        </div>
                        <div class="form-control-ux ${floatClass}">
                            <div class="inner-cell">
                                ${
                                  validationType === "text" &&
                                  (this.formValidations && this.formValidations.$submitted ||
                                    this.formValidations && this.formValidations[id].$touched) &&
                                  this.formValidations && this.formValidations[id].$invalid
                                    ? `<div>
                                    <span class="f-smaller">{{ 'invalid-field' | translate }}</span>
                                </div>`
                                    : ""
                                }
                            <div>
                        </div>
                    </div>
                </div>
            </div>
        `;
        break;
      default:
        template = `
            <div class="form-wrapper-ux">
                <div class="form-row-ux">
                    <div class="form-control-ux ${labelWidthClass}">
                        <div class="inner-cell ${labelAlignClass}">
                            <label for="{{id}}" class="form-lb">{{ label | translate }} default</label>
                        </div>
                    </div>
                    <div class="form-control-ux ${controlWidthClass}">
                        <div class="inner-cell">
                            ${
                              validationType === "text" &&
                              (this.formValidations && this.formValidations.$submitted ||
                                this.formValidations && this.formValidations[id].$touched) &&
                              this.formValidations && this.formValidations[id].$invalid
                                ? `<div>
                                <span class="f-smaller">{{ 'invalid-field' | translate }}</span>
                            </div>`
                                : ""
                            }
                        <div>
                    </div>
                </div>
            </div>
        `;
    }

    return template;
  }

  getFloatElement(labelAlign) {
    var result = "pull-left";

    switch (labelAlign) {
      case "r":
      case "right":
        result = "pull-right";
        break;
      default:
        result = "pull-left";
    }
    return result;
  }

  getWidthValue(oldValue) {
    if (oldValue && Number(oldValue) !== "number") {
      // Obtención del valor más cercano dentro del array de tamaños de los estilos existentes
      var widthValues = [10, 15, 20, 25, 30, 40, 45, 50, 70, 80, 85, 90, 100];
      var value = widthValues[0];
      var diff = Math.abs(oldValue - value);

      for (var val = 0; val < widthValues.length; val++) {
        var newdiff = Math.abs(oldValue - widthValues[val]);
        if (newdiff < diff) {
          diff = newdiff;
          value = widthValues[val];
        }
      }
      return value + "-pc"; // class = 'w-70-pc'
    } else {
      return "auto"; // class = 'w-auto'
    }
  }

  getAlignValue(labelAlign) {
    var result = "l";

    if (angular.isDefined(labelAlign)) {
      switch (labelAlign) {
        case "r":
        case "right":
          result = "r";
          break;
        case "c":
        case "center":
          result = "c";
          break;
        default:
          result = "l";
      }
    }
    return result;
  }
}

(function() {
  customElements.define("stern-input-form", sternInputForm);
})();
